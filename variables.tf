variable "access_key" {
	default = "****"
}
variable "secret_key" {
	default = "*****"
}
variable "region" {
    default = "eu-west-3"
}
variable "workspace" {
    default = "user"
}
variable "bucket" {
    default = "bucket"
}
variable "password" {
    default = "*****"
}
variable "aws_type" {
    default = "t2.micro"
}
variable "aws_ami" {
    default = "ami-00798d7180f25aac2"
}