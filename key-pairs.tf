resource "aws_key_pair" "deployer" {
  key_name = "deploy-${var.workspace}"
  public_key = "${file("${path.module}/id_rsa.pub")}"
}